{-# LANGUAGE ForeignFunctionInterface #-}

module ReadDouble where

import qualified Data.ByteString as S
import qualified Data.ByteString.Char8 as B
import           Data.ByteString.Internal (inlinePerformIO)
import           Data.ByteString.Lazy.Char8 (ByteString)
import qualified Data.ByteString.Lazy.Char8 as L
import           Data.ByteString.Lex.Fractional
import           Data.Maybe
import           Foreign
import           Foreign.C.Types
import           System.IO.Unsafe

strtod_unsafe' :: B.ByteString -> Double
strtod_unsafe' s = unsafePerformIO $ B.useAsCString s $ \cstr ->
  realToFrac `fmap` c_strtod cstr nullPtr

strtod_unsafe :: ByteString -> Double
strtod_unsafe ls = unsafePerformIO $ B.useAsCString s $ \cstr ->
  realToFrac `fmap` c_strtod cstr nullPtr
  where
    s = B.concat . L.toChunks $ ls

strtod_inline' :: B.ByteString -> Double
strtod_inline' s = inlinePerformIO $ B.useAsCString s $ \cstr ->
  realToFrac `fmap` c_strtod cstr nullPtr

strtod_inline :: ByteString -> Double
strtod_inline ls = inlinePerformIO $ B.useAsCString s $ \cstr ->
  realToFrac `fmap` c_strtod cstr nullPtr
  where
    s = B.concat . L.toChunks $ ls

foreign import ccall unsafe "static stdlib.h strtod" c_strtod
 :: Ptr CChar -> Ptr (Ptr CChar) -> IO CDouble

lexdouble :: B.ByteString -> Double
lexdouble = fst . fromJust . readSigned readDecimal
