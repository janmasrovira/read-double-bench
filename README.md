# Results #


```
#!text

$ stack exec rdb

benchmarking read double/strtod-unsafe-lazy
time                 389.1 ns   (383.1 ns .. 397.0 ns)
                     0.997 R²   (0.993 R² .. 1.000 R²)
mean                 386.0 ns   (382.1 ns .. 395.2 ns)
std dev              18.80 ns   (8.337 ns .. 37.96 ns)
variance introduced by outliers: 67% (severely inflated)

benchmarking read double/strtod-unsafe-strict
time                 330.8 ns   (329.5 ns .. 332.7 ns)
                     0.999 R²   (0.996 R² .. 1.000 R²)
mean                 341.5 ns   (335.3 ns .. 356.8 ns)
std dev              32.06 ns   (16.41 ns .. 55.39 ns)
variance introduced by outliers: 89% (severely inflated)

benchmarking read double/strtod-inline-lazy
time                 291.5 ns   (288.7 ns .. 294.8 ns)
                     0.999 R²   (0.999 R² .. 1.000 R²)
mean                 291.7 ns   (289.9 ns .. 293.8 ns)
std dev              7.165 ns   (6.095 ns .. 8.658 ns)
variance introduced by outliers: 34% (moderately inflated)

benchmarking read double/strtod-inline-strict
time                 243.5 ns   (241.7 ns .. 245.4 ns)
                     0.999 R²   (0.999 R² .. 1.000 R²)
mean                 242.1 ns   (240.5 ns .. 245.7 ns)
std dev              7.883 ns   (4.108 ns .. 15.12 ns)
variance introduced by outliers: 48% (moderately inflated)

benchmarking read double/bs-lexing
time                 316.0 ns   (312.4 ns .. 320.7 ns)
                     0.999 R²   (0.998 R² .. 0.999 R²)
mean                 319.6 ns   (315.9 ns .. 322.8 ns)
std dev              10.58 ns   (8.964 ns .. 12.87 ns)
variance introduced by outliers: 49% (moderately inflated)
```