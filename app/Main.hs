module Main where

import           Criterion.Main
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Internal as I
import           Data.ByteString.Lazy.Char8
import           ReadDouble

d1 :: Double
d1 = -456.5645665153

b1 :: ByteString
b1 = pack $ show d1

b1' :: I.ByteString
b1' = B.pack $ show d1

main :: IO ()
main = defaultMain [
  bgroup "read double" [
      bench "strtod-unsafe-lazy" $ whnf strtod_unsafe b1
      , bench "strtod-unsafe-strict" $ whnf strtod_unsafe' b1'
      , bench "strtod-inline-lazy" $ whnf strtod_inline b1
      , bench "strtod-inline-strict" $ whnf strtod_inline' b1'
      , bench "bs-lexing" $ whnf lexdouble b1'
      ]
  ]
